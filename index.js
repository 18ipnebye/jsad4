"use strict";

const cards = document.querySelector('.cards')

const xhr = new XMLHttpRequest();

    fetch('https://ajax.test-danit.com/api/swapi/films')
        .then(res => res.json())
        .then(data => {
            data.forEach(el => {
                const card = document.createElement("div");
                card.classList.add("card");

                const name = document.createElement("h2");
                const episodeID = document.createElement("span");
                const opening = document.createElement("h5");
                const ul = document.createElement("ul");

                ul.innerHTML = `
                <h4>Characters:</h4>
                `

                name.innerText = el.name;
                card.append(name);

                episodeID.innerText = `Episode: ${el.episodeId}`;
                card.append(episodeID);

                opening.innerText = `Opening: ${el.openingCrawl}`;
                card.append(opening);


                el.characters.forEach((el) => {
                    fetch(`${el}`)
                    .then(res => res.json())
                    .then(data => {
                        ul.insertAdjacentHTML("beforeend", `
                        <li>${data.name}</li>
                        `)
                    })
                })

                card.append(ul);

                cards.append(card);
            });
        })